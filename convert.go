package main

import (
	"encoding/json"
	"fmt"
	"io"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/brakeman/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/ruleset"
)

const (
	brakemanDocURLPrefix = "https://brakemanscanner.org/docs/warning_types/"
)

var cveRegexp = regexp.MustCompile(`(CVE-\d{4}-\d{4,7})`)

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var doc = struct {
		Warnings []Warning `json:"warnings"`
	}{}

	err := json.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	issues := []issue.Issue{}
	for _, w := range doc.Warnings {
		// Ignore vulnerabilities affecting project dependencies
		// because these are already reported by GitLab Dependency Scanning.
		if !w.AffectsDependency() {
			issues = append(issues, issue.Issue{
				Category: metadata.Type,
				Scanner:  metadata.IssueScanner,
				// w.Message is free of contextual data when vulnerability is not affecting dependencies
				Name:        w.Message,
				Message:     w.Message,
				Confidence:  w.ConfidenceLevel(),
				Severity:    w.SeverityLevel(),
				CompareKey:  w.Fingerprint,
				Location:    w.ConvertLocation(prependPath),
				Identifiers: w.Identifiers(),
				Links:       w.Links(),
			})
		}
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	report.Analyzer = metadata.AnalyzerID
	report.Config.Path = ruleset.PathSAST
	return &report, nil
}

// Warning describes a vulnerability found in the source code.
type Warning struct {
	WarningType string      `json:"warning_type"`
	WarningCode int         `json:"warning_code"`
	Fingerprint string      `json:"fingerprint"`
	CheckName   string      `json:"check_name"`
	Message     string      `json:"message"`
	File        string      `json:"file"`
	Line        int         `json:"line"`
	Link        string      `json:"link"`
	Code        string      `json:"code"`
	RenderPath  interface{} `json:"render_path"`
	Location    struct {
		Type   string `json:"type"`
		Class  string `json:"class"`
		Method string `json:"method"`
	} `json:"location"`
	UserInput  string `json:"user_input"`
	Confidence string `json:"confidence"`
}

// AffectsDependency tells whether this vulnerability affects a gem the project depends on.
func (w Warning) AffectsDependency() bool {
	return filepath.Base(w.File) == "Gemfile.lock"
}

// ConfidenceLevel returns the normalized confidence
// See https://github.com/presidentbeef/brakeman/blob/v4.3.1/lib/brakeman/warning.rb#L13-L17
func (w Warning) ConfidenceLevel() issue.ConfidenceLevel {
	switch w.Confidence {
	case "High":
		return issue.ConfidenceLevelHigh
	case "Medium":
		return issue.ConfidenceLevelMedium
	case "Weak":
		return issue.ConfidenceLevelLow
	}
	return issue.ConfidenceLevelUnknown
}

// SeverityLevel returns the normalized severity
// This level is a remapping of confidence as described in the docs:
// https://github.com/presidentbeef/brakeman#confidence-levels
func (w Warning) SeverityLevel() issue.SeverityLevel {
	switch w.Confidence {
	case "High":
		return issue.SeverityLevelHigh
	case "Medium":
		return issue.SeverityLevelMedium
	case "Weak":
		return issue.SeverityLevelLow
	}
	return issue.SeverityLevelUnknown
}

// Links returns the normalized links of the vulnerability.
func (w Warning) Links() []issue.Link {
	// If Warning's Link points to Brakeman documentation, do not add it
	// here as it's already included in the Brakeman Identifier.
	if strings.Contains(w.Link, brakemanDocURLPrefix) || w.Link == "" {
		return []issue.Link{}
	}

	return []issue.Link{
		issue.Link{
			URL: w.Link,
		},
	}
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (w Warning) Identifiers() []issue.Identifier {
	identifiers := []issue.Identifier{
		w.BrakemanIdentifier(),
	}

	// Try to find CVE-ID within Message
	cve := cveRegexp.FindString(w.Message)
	if cve != "" {
		identifiers = append(identifiers, issue.CVEIdentifier(cve))
	}

	return identifiers
}

// BrakemanIdentifier returns a structured Identifier for a brakeman Warning Code
func (w Warning) BrakemanIdentifier() issue.Identifier {
	var url string
	// If Link points to Brakeman documentation, use it as Identifier Url.
	if strings.Contains(w.Link, brakemanDocURLPrefix) {
		url = w.Link
	}
	// TODO: else { build url if available on https://brakemanscanner.org/docs/warning_types }

	return issue.Identifier{
		Type:  "brakeman_warning_code",
		Name:  fmt.Sprintf("Brakeman Warning Code %d", w.WarningCode),
		Value: strconv.Itoa(w.WarningCode),
		URL:   url,
	}
}

// ConvertLocation returns a structured Location
func (w Warning) ConvertLocation(prependPath string) issue.Location {

	location := issue.Location{
		File:      filepath.Join(prependPath, w.File),
		LineStart: w.Line,
	}

	// Only take locations with "Method" type
	if w.Location.Type == "method" {
		location.Class = w.Location.Class
		location.Method = w.Location.Method
	}

	return location
}
