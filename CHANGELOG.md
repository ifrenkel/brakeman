# Brakeman analyzer changelog

## v2.12.0
- Update common to v2.22.0 (!52)
- Update urfave/cli to v2.3.0 (!52)

## v2.11.0
- Update common and enabled disablement of rulesets (!50)

## v2.10.1
- Reclassify confidence level as severity (!49)

## v2.10.0
- Update brakeman scanner to v4.10.0 (!48)
- Update golang dependencies

## v2.9.2
- Update `metadata.ScannerVersion` to match brakeman version `4.9.1` (!43)

## v2.9.1
- Update golang dependencies (!42)

## v2.9.0
- Update brakeman to [v4.9.1](https://brakemanscanner.org/blog/2020/09/04/brakeman-4-dot-9-dot-1-released) (!40)
- Update golang dependencies (!40)

## v2.8.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!39)

## v2.7.0
- Switch to Alpine Linux docker container (!38)
- Update golang to v1.15

## v2.6.0
- Add scan object to report (!34)

## v2.5.0
- Bump brakeman to v4.9.0 (!33)

## v2.4.1
- Add `app.Version` to support `scan.scanner.version` field in security reports (!32)

## v2.4.0
- Switch to the MIT Expat license (!28)

## v2.3.1
- Update Debug output to give a better description of command that was ran (!30)

## v2.3.0
- Update logging to be standardized across analyzers (!29)

## v2.2.1
- Use `ruby:2.7-slim` (Debian Buster Slim) as a base Docker image (!27)
- Remove `location.dependency` from the generated SAST report (!26)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!21)

## v2.1.0
- Add support for custom CA certs (!18)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Bump brakeman to 4.3.1
- Don't detect a Rails application unless "rails" is a first level dependency
- Shows brakeman command error output
- Don't ignore Brakeman exit code

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
